use std::io::BufRead;
use std::io::Lines;

pub fn process_input(lines_iter: Lines<impl BufRead>) -> i32 {
    let mut sum: i32 = 0;
    for line in lines_iter.map(|l| l.unwrap()) {
        let number = i32::from_str_radix(&line, 10).unwrap();
        sum += fuel_required(number);
    }
    return sum;
}

/// Fuel required to launch a given module is based on its mass. Specifically, 
/// to find the fuel required for a module, take its mass, divide by three, 
/// round down, and subtract 2.
fn fuel_required(mass: i32) -> i32 {
    return mass / 3 - 2;
}

/// Fuel itself requires fuel just like a module - take its mass, divide by 
/// three, round down, and subtract 2. However, that fuel also requires fuel, 
/// and that fuel requires fuel, and so on. Any mass that would require 
/// negative fuel should instead be treated as if it requires zero fuel; the 
/// remaining mass, if any, is instead handled by wishing really hard, which 
/// has no mass and is outside the scope of this calculation.
///
/// So, for each module mass, calculate its fuel and add it to the total. Then, 
/// treat the fuel amount you just calculated as the input mass and repeat the 
/// process, continuing until a fuel requirement is zero or negative.
fn fuel_required_including_fuel(mass: i32) -> i32 {
    let mut total_fuel = 0;
    let mut fuel = fuel_required(mass);
    total_fuel += fuel;
    info!("Fuel for mass: {}", total_fuel);
    while fuel > 0 {
        fuel = fuel_required(fuel);
        total_fuel += fuel;
        info!("Added {} fuel for new total {}", fuel, total_fuel);
    }
    return total_fuel;
}

#[cfg(test)]
mod tests {
    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    macro_rules! day1a_fuel_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    init();
                    let (input, expected) = $value;
                    assert_eq!(expected, fuel_required(input));
                }
            )*
        }
    }

    macro_rules! day1b_fuel_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (input, expected) = $value;
                    assert_eq!(expected, fuel_required_including_fuel(input));
                }
            )*
        }
    }

    use day1a::*;

    day1a_fuel_tests! {
        day1a_fuel_1: (12, 2),
        day1a_fuel_2: (14, 2),
        day1a_fuel_3: (1969, 654),
        day1a_fuel_4: (100756, 33583),
    }

    day1b_fuel_tests! {
        day1b_fuel_1: (14, 2),
        day1b_fuel_2: (1969, 966),
        day1b_fuel_3: (100756, 50346),
    }
 
}
